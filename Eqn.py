#WHITE BOARD

#importing libraries and dependencies
import math
from sympy import *
import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Flatten, Dense

def nothing():
    pass

cap = cv2.VideoCapture(1)

# creating the slider to get correct thresholds
cv2.namedWindow('binary_slider')
# create trackbars
cv2.createTrackbar('bin1','binary_slider',120,255,nothing)
cv2.createTrackbar('bin2','binary_slider',120,255,nothing)

board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
# ax + by = c
# px + qy = r
a = 0
b = 0
c = 0
p = 0
q = 0
r = 0
eqFlag = 0

def showDemo():
    global a
    global b
    global c
    global p
    global q
    global r
    global board
    a = round(a)
    b = round(b)
    c = round(c)
    p = round(p)
    q = round(q)
    r = round(r)

    board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
    if eqFlag < 3:
        ans = 'Lets solve the two equations using substitution'
        xi = 8
        yi = 16
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20

        yi += 20
        ans = 'Ax + By - C = 0    -(1)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20
        ans = 'Px + Qy - R = 0    -(2)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20

        yi += 20
        ans = 'a = ' + str(a) + ' b = ' + str(b) + ' c = ' + str(c)
        paint_board(ans, xi, yi, 255, 255, 255)
        # print(ans)
        yi += 20

        yi += 20
        ans = 'p = ' + str(p) + ' q = ' + str(q) + ' r = ' + str(r)
        paint_board(ans, xi, yi, 255, 255, 255)
        # print(ans)
        yi += 20

    else:
        ans = 'Lets solve the two equations using substitution'
        xi = 8
        yi = 16
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20

        yi += 20
        ans = 'Ax + By - C = 0    -(1)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20
        ans = 'Px + Qy - R = 0    -(2)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20

        yi += 20
        ans = str(a) + 'x + ' + str(b) + 'y = ' + str(c)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20
        ans = str(p) + 'x + ' + str(q) + 'y = ' + str(r)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20

        yi += 20
        # y = (c - ax) / b
        # px + qy = px + q((c - ax) / b) = c
        ans = 'From equation 1 => y = (C - Ax)/B    -(3)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20
        ans = 'Putting y from (3) in equation (2)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20
        ans = 'Px + Qy = Px + Q((C - Ax)/B)          -(4)'
        paint_board(ans, xi, yi, 0, 255, 0)
        yi += 20
        ans = 'Px + Qy = ' + str(p) + 'x + ' + str(q) + '((' +str(c)+ ' - ' +str(a)+ 'x)/' +str(b)+ ')'
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20
        pi = p - q*a/b # x's coefficient
        pi = round(pi, 2)
        qi = q*c/b
        qi = round(qi, 2)

        ans = str(pi) + 'x + ' + str(qi) + ' = ' + str(r)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20

        if p!=0:
            x = (r - q)/p
            x = round(x, 2)
        else:
            x = 0

        yi += 20
        ans = 'Therefore, x = ' + str(x) + '    -(5)'
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20
        ans = 'Putting value of x from (5) in (3)'
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20
        y = (c - a*x) / b
        y = round(y, 2)
        ans = 'y = (' +str(c)+' - ' +str(a*x)+') / '+str(b)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20
        ans = 'y = ' + str(y)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20

        yi += 20
        ans = 'Hence, x = ' + str(x)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20
        ans = '        y = ' + str(y)
        paint_board(ans, xi, yi, 255, 255, 255)
        yi += 20


def paint_board(s, x, y, b, g, r):
    cv2.putText(board, s, (x, y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, (b,g,r), 1, cv2.FILLED)

#creating the model
def create_model():
    model = Sequential()
    model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(28,28, 3)))
    model.add(MaxPooling2D(2, 2))

    model.add(Convolution2D(32, 5, 5, activation='relu'))
    model.add(MaxPooling2D(2, 2))

    model.add(Flatten())
    model.add(Dense(1000, activation='relu'))

    model.add(Dense(18, activation='softmax'))
    return model

#loading model
model = create_model()
model.load_weights('model_mnist5.h5')

def add_doublestar(list_n):
    for i in range(0, len(list_n)):
        if list_n[i] == '^':
            list_n.insert(i, '*')
            list_n.insert(i+1, '*')
            break
    list_n.remove('^')
    return list_n

def add_star(s, frame1):
    list_n = [0]*len(s)
    for i in range(0, len(list_n)):
        list_n[i] = s[i]
    for i in range(0, len(list_n)+1):
        if list_n[i] == 'x' and list_n[i-1] != '*':
            list_n.insert(i, '*')
    return list_n

def convert(list1): 
    res = ""
    for i in range (0, len(list1)):
        res += str(list1[i])
    return(res) 

import operator
cap = cv2.VideoCapture(1)
st = ""
fullexp = " "
expr = []
exp2  = []
slop = []
inte = []
flag = False

no_of_times = 0

while(True):
    #reading frames from camera
    ret, frame1 = cap.read()
    frame2 = frame1[200:500, 50:700]
    frame = frame2.copy()
    frame_new = frame2.copy()
    uframe = frame1.copy()
    
    #finding contours
    bin1 = cv2.getTrackbarPos('bin1','binary_slider')
    bin1 = cv2.getTrackbarPos('bin2','binary_slider')
    
    ret, img = cv2.threshold(frame, bin1, 255, cv2.THRESH_BINARY_INV)
    cvt = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('hmmm', cvt)
    contours, hierarchy = cv2.findContours(cvt,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

    thisdict = {}
    
    flag=0
    noted_y=0
    mylist = []
    for ci in contours:
        (x, y, w, h)= cv2.boundingRect(ci)
        if (w>30) or (h>30):
            # cv2.rectangle(frame1,(x,y),(x+w,y+h), (255,0, 0), 3)
            mylist.append((x,y,w,h))
    
    for i in range(0, len(mylist)):
        x = mylist[i][0]
        y = mylist[i][1]
        w = mylist[i][2]
        h = mylist[i][3]
        if(w>100 or h>100):
            mylist.remove((x,y,w,h))
            break

    for i in range(0, len(mylist)):
        x = mylist[i][0]
        y = mylist[i][1]
        w = mylist[i][2]
        h = mylist[i][3]


        for j in range(0, len(mylist)):
            x1 = mylist[j][0]
            y1 = mylist[j][1]
            w1 = mylist[j][2]
            h1 = mylist[j][3]

            if abs(x1-x)<10 and y1 != y:
                flag = 1
                mylist.remove((x1,y1,w1,h1))
                break
            
        if flag is 1:
            break

    for i in range(0, len(mylist)):
        x = mylist[i][0]
        y = mylist[i][1]
        w = mylist[i][2]
        h = mylist[i][3]
        x-=20
        y-=20
        w+=40
        h+=40
        cv2.rectangle(frame1,(x+50,y+200),(x+w+50,y+h+200), (0,0, 255), 2)
        img1 = uframe[y+200:y+h+200, x+50:x+w+50]
        bin2 = cv2.getTrackbarPos('bin2','binary_slider')
        ret, gray = cv2.threshold(img1, bin2, 255, cv2.THRESH_BINARY)
        cv2.imshow('gray', gray)
        # gray = img1
        try:
            im = cv2.resize(gray, (40,40))
            im1 = cv2.resize(im, (28,28))
            next2 = cv2.resize(im1, (28,28))
            # gray1 = next2
            ret, gray1 = cv2.threshold(next2, bin2, 255, cv2.THRESH_BINARY)
            # cv2.imshow('well well', gray1)
            ar = np.array(gray1).reshape((28,28,3))
            ar = np.expand_dims(ar, axis=0)
            prediction = model.predict(ar)[0]

            #predicrion of class labels
            for i in range(0,19):
                if prediction[i]==1.0:
                    if i==0:
                        j= "+"
                    if i==1:
                        j= "-"
                    if i==2:
                        j= "0"
                    if i==3:
                        j= "1"
                    if i==4:
                        j= "2"
                    if i==5:
                        j= "3"
                    if i==6:
                        j= "4"
                    if i==7:
                        j= "5"
                    if i==8:
                        j= "6"
                    if i==9:
                        j= "7"
                    if i==10:
                        j= "8"
                    if i==11:
                        j= "9"
                    if i==12:
                        j= "="
                    if i==13:
                        j= "^"
                    if i==14:
                        j= "/"
                    if i==15:
                        j= "X"
                    if i==16:
                        j= "x"
                    if i==17:
                        j= "y" 
                    cv2.putText(frame1, j, (x+270,y+50), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2, cv2.LINE_AA)
                    thisdict[x]= str(j)
        except:
            d=0
    
    sort = sorted(thisdict.items(), key=operator.itemgetter(0))
    s = ""
    for x in range(0,len(sort)):
        s=s+str(sort[x][1])
    cv2.putText(frame1, s, (70,80), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 2, cv2.LINE_AA)  
    
    if len(s)>=7:
        try:
            if eqFlag == 1:
                a = int(s[0])
                b = int(s[3])
                c = int(s[6])
            if eqFlag == 2:
                p = int(s[0])
                q = int(s[3])
                r = int(s[6])
        except:
            d = 0


    if cv2.waitKey(1) & 0xFF == ord('v'):
        eqFlag = 1
    if cv2.waitKey(1) & 0xFF == ord('b'):
        eqFlag = 2
    if cv2.waitKey(1) & 0xFF == ord('n'):
        eqFlag = 3
    if cv2.waitKey(1) & 0xFF == ord('x'):
        eqFlag = 0
    
    showDemo()

    cv2.imshow('Demo', board)
    cv2.imshow('frame', frame1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break        
cap.release()
